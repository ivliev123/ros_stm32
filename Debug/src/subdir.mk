################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/syscalls.c \
../src/system_stm32f1xx.c 

CPP_SRCS += \
../src/main.cpp \
../src/stm32f1xx_hal_msp.cpp \
../src/stm32f1xx_it.cpp 

OBJS += \
./src/main.o \
./src/stm32f1xx_hal_msp.o \
./src/stm32f1xx_it.o \
./src/syscalls.o \
./src/system_stm32f1xx.o 

C_DEPS += \
./src/syscalls.d \
./src/system_stm32f1xx.d 

CPP_DEPS += \
./src/main.d \
./src/stm32f1xx_hal_msp.d \
./src/stm32f1xx_it.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: MCU G++ Compiler'
	@echo $(PWD)
	arm-none-eabi-g++ -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -std=c++1y -DSTM32 -DSTM32F1 -DSTM32F103C8Tx -DDEBUG -DSTM32F103xB -DUSE_HAL_DRIVER -I"/home/ivliev/workspace/blink2/HAL_Driver/Inc/Legacy" -I"/home/ivliev/workspace/blink2/inc" -I"/home/ivliev/workspace/blink2/CMSIS/device" -I"/home/ivliev/workspace/blink2/CMSIS/core" -I"/home/ivliev/workspace/blink2/HAL_Driver/Inc" -I/home/ivliev/workspace/blink2/ros_lib -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fno-exceptions -fno-rtti -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -mfloat-abi=soft -DSTM32 -DSTM32F1 -DSTM32F103C8Tx -DDEBUG -DSTM32F103xB -DUSE_HAL_DRIVER -I"/home/ivliev/workspace/blink2/HAL_Driver/Inc/Legacy" -I"/home/ivliev/workspace/blink2/inc" -I"/home/ivliev/workspace/blink2/CMSIS/device" -I"/home/ivliev/workspace/blink2/CMSIS/core" -I"/home/ivliev/workspace/blink2/HAL_Driver/Inc" -I/home/ivliev/workspace/blink2/ros_lib -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


